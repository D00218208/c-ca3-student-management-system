#include <iostream>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>  
#include <vector>
#include <algorithm>  
#include "Student.h"
using namespace std;
void readStudents();
void printChoices();
void addStudent();
void showAllStudents();
void parseLine(const string& str);
void showStudentsOnCourse();
void findStudent();
void showFailingStudents();
double calculateAverage(int* scores);
void editStudent(Student student);
bool compareIDAscending(Student s1, Student s2);
void writeStudents();

vector<Student> students;

int main()
{
    readStudents();

    printChoices();

    bool finished = false;

    while (finished == false)
    {
        int choice;

        cout << "\nEnter a choice (enter 6 for list): ";
        cin >> choice;

        switch (choice)
        {
        case 1:
            addStudent();
            break;
        case 2:
            showAllStudents();
            break;
        case 3:
            showStudentsOnCourse();
            break;
        case 4:
            findStudent();
            break;
        case 5:
            showFailingStudents();
            break;
        case 6:
            printChoices();
            break;
        case 7:
            writeStudents();
            finished = true;
            continue;
        default:
            cin.clear();
            cin.ignore(1000, '\n');
            cout << "Please enter one of the listed choices" << endl;
            break;
        }
    }
}

/*
* Prompts the user to enter the attributes of a new student which
* are validated and used to create a new student object.
*/
void addStudent()
{
    string title, name, course;
    int studentID, s1, s2, s3, s4;

    cout << "\nEnter the student's ID: ";
    cin >> studentID;

    while (studentID < 1) 
    {
        cin.clear();
        cin.ignore(1000, '\n');
        cout << "The student ID is invalid, please re-enter: ";
        cin >> studentID;
    }

    cin.clear();
    cin.ignore(1000, '\n');
    cout << "\nEnter the student's title: ";
    getline(cin, title);

    while (title.length() < 2)
    {
        cout << "The title is invalid, please re-enter: ";
        getline(cin, title);
    }

    cout << "\nEnter the student's name: ";
    getline(cin, name);

    while (name.length() < 4)
    {
        cout << "The name is invalid, please re-enter: ";
        getline(cin, name);
    }

    cout << "\nEnter the student's first score: ";
    cin >> s1;

    while (s1 < 0 || s1 > 100 || cin.fail())
    {
        cin.clear();
        cin.ignore(1000, '\n');
        cout << "The score is invalid, please re-enter: ";
        cin >> s1;
    }
  
    cout << "\nEnter the student's second score: ";
    cin >> s2;

    while (s2 < 0 || s2 > 100 || cin.fail())
    {
        cin.clear();
        cin.ignore(1000, '\n');
        cout << "The score is invalid, please re-enter: ";
        cin >> s2;
    }

    cout << "\nEnter the student's third score: ";
    cin >> s3;

    while (s3 < 0 || s3 > 100 || cin.fail())
    {
        cin.clear();
        cin.ignore(1000, '\n');
        cout << "The score is invalid, please re-enter: ";
        cin >> s3;
    }

    cout << "\nEnter the student's fourth score: ";
    cin >> s4;

    while (s4 < 0 || s4 > 100 || cin.fail())
    {
        cin.clear();
        cin.ignore(1000, '\n');
        cout << "The score is invalid, please re-enter: ";
        cin >> s4;
    }

    cin.clear();
    cin.ignore(1000, '\n');
    cout << "\nEnter the student's course: ";
    getline(cin, course);

    while (course.length() < 4)
    {
        cout << "The course is invalid, please re-enter: ";
        getline(cin, course);
    }

    Student student(studentID, title, name, s1, s2, s3, s4, course);
    students.push_back(student);
    sort(students.begin(), students.end(), compareIDAscending);
}

/*
* Compares the ID of two student objects and returns true if the
* first student's ID is less than the second student's ID
*/
bool compareIDAscending(Student s1, Student s2)
{
    return (s1.getStudentID() < s2.getStudentID());
}

/*
* Prints out the details of every student in the students vector
*/
void showAllStudents()
{
    int count = 0;

    vector<Student>::iterator iter = students.begin();
    for (iter = students.begin(); iter != students.end(); iter++)
    {
        count++;
        cout << "\n";
        cout << *iter;
    }

    if (count == 0)
    {
        cout << "No students have been found" << endl;
    }
}

/*
* Allows the user to enter the name of a course and outputs the
* names and IDs of the students on the course. It also displays
* the number of students on the course
*/
void showStudentsOnCourse()
{
    string course;
    int count = 0;
    cout << "Enter the name of the course you want to show the students of: ";
    cin >> course;

    vector<Student>::iterator iter = students.begin();
    for (iter = students.begin(); iter != students.end(); iter++)
    {
        if (iter->getCourse() == course)
        {
            count++;
            cout << "\n";
            cout << "Student ID: " << iter->getStudentID() << endl;
            cout << "Name: " << iter->getName() << endl;
        }
    }

    if (count == 0)
    {
        cout << "No students have been found for this course" << endl;
    }
    else
    {
        cout << "There are " << count << " students on this course" << endl;
    }
}

/*
* Allows the user to enter a student ID and shows the details
* of the student matching the inputted ID if they are found.
*/
void findStudent()
{
    int studentID;
    int editDetails;
    bool found = false;
    cout << "Enter the ID of the student you want to find: ";
    cin >> studentID;

    while (cin.fail())
    {
        cin.clear();
        cin.ignore(1000, '\n');
        cout << "The ID entered is not a number, please re-enter: ";
        cin >> studentID;
    }

    vector<Student>::iterator iter = students.begin();
    for (iter = students.begin(); iter != students.end(); iter++)
    {
        if (iter->getStudentID() == studentID)
        {
            found = true;
            cout << "\n";
            cout << *iter;

            cout << "\nWould you like to edit this student's details?" << endl;
            cout << "1 - Yes" << endl;
            cout << "2 - No" << endl;
            cout << "\nEnter a choice: ";
            cin >> editDetails;

            while (editDetails != 1 && editDetails != 2)
            {
                cin.clear();
                cin.ignore(1000, '\n');
                cout << "That choice is invalid, please enter 1 to edit or 2 to finish: ";
                cin >> editDetails;
            }

            if (editDetails == 1)
            {
                editStudent(*iter);
            }
        }
    }

    if (found == false)
    {
        cout << "No student matching that ID has been found" << endl;
    }
}

/*
* Takes in a student object and allows the user to enter input
* to determine which of the student's attributes they want to 
* change
*/
void editStudent(Student student)
{
    cout << "1 - edit student ID" << endl;
    cout << "2 - edit title" << endl;
    cout << "3 - edit name" << endl;
    cout << "4 - edit scores" << endl;
    cout << "5 - edit course" << endl;
    cout << "6 - review this student's details" << endl;
    cout << "7 - Print list of available choices again" << endl;
    cout << "8 - finish editing" << endl;

    bool finished = false;
    string title, name, course;
    int studentID, s1, s2, s3, s4;

    while (finished == false)
    {
        int choice;

        cout << "\nEnter a choice (enter 7 for list): ";
        cin >> choice;

        switch (choice)
        {
        case 1:
            cout << "\nEnter the student's ID: ";
            cin >> studentID;
            while (studentID < 1 || cin.fail())
            {
                cin.clear();
                cin.ignore(1000, '\n');
                cout << "The student ID is invalid, please re-enter: ";
                cin >> studentID;
            }
            student.setStudentID(studentID);
            sort(students.begin(), students.end(), compareIDAscending);
            break;
        case 2:
            cin.clear();
            cin.ignore(1000, '\n');
            cout << "\nEnter the student's title: ";
            getline(cin, title);

            while (title.length() < 2)
            {
                cout << "The title is invalid, please re-enter: ";
                getline(cin, title);
            }
            student.setTitle(title);
            break;
        case 3:
            cin.clear();
            cin.ignore(1000, '\n');
            cout << "\nEnter the student's name: ";
            getline(cin, name);

            while (name.length() < 4)
            {
                cout << "The name is invalid, please re-enter: ";
                getline(cin, name);
            }
            student.setName(name);
            break;
        case 4:
            cout << "\nEnter the student's first score: ";
            cin >> s1;

            while (s1 < 0 || s1 > 100 || cin.fail())
            {
                cin.clear();
                cin.ignore(1000, '\n');
                cout << "The score is invalid, please re-enter: ";
                cin >> s1;
            }

            cout << "\nEnter the student's second score: ";
            cin >> s2;

            while (s2 < 0 || s2 > 100 || cin.fail())
            {
                cin.clear();
                cin.ignore(1000, '\n');
                cout << "The score is invalid, please re-enter: ";
                cin >> s2;
            }

            cout << "\nEnter the student's third score: ";
            cin >> s3;

            while (s3 < 0 || s3 > 100 || cin.fail())
            {
                cin.clear();
                cin.ignore(1000, '\n');
                cout << "The score is invalid, please re-enter: ";
                cin >> s3;
            }

            cout << "\nEnter the student's fourth score: ";
            cin >> s4;

            while (s4 < 0 || s4 > 100 || cin.fail())
            {
                cin.clear();
                cin.ignore(1000, '\n');
                cout << "The score is invalid, please re-enter: ";
                cin >> s4;
            }
            student.setScores(s1, s2, s3, s4);
            break;
        case 5:
            cin.clear();
            cin.ignore(1000, '\n');
            cout << "\nEnter the student's course: ";
            getline(cin, course);

            while (course.length() < 4)
            {
                cout << "The course is invalid, please re-enter: ";
                getline(cin, title);
            }
            student.setCourse(course);
            break;
        case 6:
            cout << student;
            break;
        case 7:
            cout << "1 - edit student ID" << endl;
            cout << "2 - edit title" << endl;
            cout << "3 - edit name" << endl;
            cout << "4 - edit scores" << endl;
            cout << "5 - edit course" << endl;
            cout << "6 - review this student's details" << endl;
            cout << "7 - Print list of available choices again" << endl;
            cout << "8 - finish editing" << endl;
            break;
        case 8:
            finished = true;
            continue;
        default:
            cin.clear();
            cin.ignore(1000, '\n');
            cout << "Please enter one of the listed choices" << endl;
            break;
        }
    }
}

/*
* Shows a list of students who have an average score of less than
* 40. This method calls the calculateAverage method to determine
* the average.
*/
void showFailingStudents()
{
    int count = 0;

    vector<Student>::iterator iter = students.begin();
    for (iter = students.begin(); iter != students.end(); iter++)
    {
        if (calculateAverage(iter->getScores()) < 40)
        {
            count++;
            cout << "\n";
            cout << *iter << endl;
        }
    }

    if (count == 0)
    {
        cout << "No failing students have been found" << endl;
    }
}

/*
* Takes in a pointer to an array of four student scores and
* calculates the average of the scores.
*/
double calculateAverage(int *scores)
{
    int total = scores[0] + scores[1] + scores[2] + scores[3];
    double average = total / 4;
    return average;
}

/*
* Parses each line of a text file by gathering each attribute and
* using them to create a student object.
*/
void parseLine(const string& str) 
{
    stringstream strStream(str);
    int studentID = 0;

    try
    {
        string str;
        getline(strStream, str, ';');

        studentID = stoi(str);

        string title;
        getline(strStream, title, ';');

        string name;
        getline(strStream, name, ';');

        int s1 = 0;
        int s2 = 0;
        int s3 = 0;
        int s4 = 0;

        int scores = 0;

        getline(strStream, str, ';');

        scores = stoi(str);

        s1 = (scores >> 24) & 255;
        s2 = (scores >> 16) & 255;
        s3 = (scores >> 8) & 255;
        s4 = (scores) & 255;

        string course;
        getline(strStream, course, '\n');
        Student student(studentID, title, name, s1, s2, s3, s4, course);
        students.push_back(student);
    }
    catch (std::invalid_argument const& e)
    {
        cout << "Bad input: std::invalid_argument thrown" << '\n';
    }
    catch (std::out_of_range const& e)
    {
        cout << "Integer overflow: std::out_of_range thrown" << '\n';
    }
}

/*
* Opens a text file of students, parses each line into a student
* object and adds them to the vector of student objects. 
*/
void readStudents() 
{
    cout << "Reading from students.txt" << endl;

    string line;
    ifstream inStream("students.txt");

    if (inStream.good())
    {
        while (getline(inStream, line)) 
        {
            parseLine(line);
        }
        inStream.close();
        sort(students.begin(), students.end(), compareIDAscending);
        cout << "Students loaded from file successfully" << endl;
    }
    else
    {
        cout << "Unable to open file, or file is empty";
    }
}

/*
* Iterates through the vector of Student objects, gathers the attributes
* of each student and writes them to a file, line by line.
*/
void writeStudents()
{
    cout << "Creating and writing to file: students.txt" << endl;

    ofstream outStream("students.txt");

    if (outStream.good())
    {
        vector<Student>::iterator iter = students.begin();
        for (iter = students.begin(); iter != students.end(); iter++)
        {
            int studentID = iter->getStudentID();
            string title = iter->getTitle();
            string name = iter->getName();

            int scores = 0;
            int* studentScores = iter->getScores();

            scores = scores | (studentScores[0] << 24);
            scores = scores | (studentScores[1] << 16);
            scores = scores | (studentScores[2] << 8);
            scores = scores | (studentScores[3]);

            string course = iter->getCourse();

            outStream << studentID << ";" << title << ";" << name << ";" << scores << ";" << course << endl;       
        }

        outStream.close();
        cout << "Students written to file successfully" << endl;
    }
    else
    {
        cout << "Unable to open file";
    }
}

/*
* Prints a list of choices available to the user while they
* are using the menu.
*/
void printChoices()
{
    cout << "\n1 - Add a student" << endl;
    cout << "2 - Show all students" << endl;
    cout << "3 - Show students on a course" << endl;
    cout << "4 - Find a student" << endl;
    cout << "5 - Show failing students" << endl;
    cout << "6 - Print list of available choices again" << endl;
    cout << "7 - Exit the application" << endl;
}