#include "Student.h"

Student::Student(int studentID, string title, string name, int s1, int s2, int s3, int s4, string course) 
{
	this->studentID = studentID;
	this->title = title;
	this->name = name;

	scores = new int[4];
	scores[0] = s1;
	scores[1] = s2;
	scores[2] = s3;
	scores[3] = s4;

	this->course = course;
}

Student::Student()
{
	this->studentID = 1234;
	this->title = "default";
	this->name = "default";

	scores = new int[4];
	scores[0] = 0;
	scores[1] = 0;
	scores[2] = 0;
	scores[3] = 0;

	this->course = "default";
}

Student::Student(const Student& source)
{
	this->studentID = source.studentID;
	this->title = source.title;
	this->name = source.name;
	this->scores = new int[4];
	scores[0] = source.scores[0];
	scores[1] = source.scores[1];
	scores[2] = source.scores[2];
	scores[3] = source.scores[3];
	this->course = source.course;
}

int Student::getStudentID()
{
	return this->studentID;
}

string Student::getTitle()
{
	return this->title;
}

string Student::getName()
{
	return this->name;
}

int* Student::getScores()
{
	return this->scores;
}

string Student::getCourse()
{
	return this->course;
}

void Student::setStudentID(int studentID)
{
	this->studentID = studentID;
}

void Student::setTitle(string title)
{
	this->title = title;
}

void Student::setName(string name)
{
	this->name = name;
}

void Student::setScores(int s1, int s2, int s3, int s4)
{
	this->scores[0] = s1;
	this->scores[1] = s2;
	this->scores[2] = s3;
	this->scores[3] = s4;
}

void Student::setCourse(string course)
{
	this->course = course;
}

Student::~Student()
{
	delete[] scores;
}

Student& Student::operator= (const Student& otherStudent)
{
	if (this == &otherStudent)
	{
		return *this;
	}	

	studentID = otherStudent.studentID;
	title = otherStudent.title;
	name = otherStudent.name;
	scores = new int[4];
	for (int i = 0; i < 4; i++)
	{
		scores[i] = otherStudent.scores[i];
	}
	course = otherStudent.course;

	return *this;
}

int Student::operator == (Student& otherStudent)
{
	if (this->studentID != otherStudent.studentID)
	{
		return 0;
	}
	else if (this->title != otherStudent.title)
	{
		return 0;
	}
	else if (this->name != otherStudent.name)
	{
		return 0;
	}
	else if (this->scores[0] != otherStudent.scores[0])
	{
		return 0;
	}
	else if (this->scores[1] != otherStudent.scores[1])
	{
		return 0;
	}
	else if (this->scores[2] != otherStudent.scores[2])
	{
		return 0;
	}
	else if (this->scores[3] != otherStudent.scores[3])
	{
		return 0;
	}
	else if (this->course != otherStudent.course)
	{
		return 0;
	}

	return 1;
}

int Student::operator != (Student& otherStudent)
{
	if (this->studentID == otherStudent.studentID)
	{
		return 0;
	}
	else if (this->title == otherStudent.title)
	{
		return 0;
	}
	else if (this->name == otherStudent.name)
	{
		return 0;
	}
	else if (this->scores[0] == otherStudent.scores[0])
	{
		return 0;
	}
	else if (this->scores[1] == otherStudent.scores[1])
	{
		return 0;
	}
	else if (this->scores[2] == otherStudent.scores[2])
	{
		return 0;
	}
	else if (this->scores[3] == otherStudent.scores[3])
	{
		return 0;
	}
	else if (this->course == otherStudent.course)
	{
		return 0;
	}

	return 1;
}

ostream& operator<< (ostream& out, const Student& student)
{
	cout << "Student details: " << endl;
	cout << "Student ID: " << student.studentID << endl;
	cout << "Title: " << student.title << endl;
	cout << "Name: " << student.name << endl;
	cout << "Scores: " << student.scores[0] << "," << student.scores[1] << "," << student.scores[2] << "," << student.scores[3] << endl;
	cout << "Course: " << student.course << endl;
	return out;
}

istream& operator>> (istream& in, Student& student)
{
	cout << "Enter student details: " << endl;
	cout << "Enter student ID: ";
	cin >> student.studentID;
	cout << "Enter title: ";
	cin >> student.title;
	cout << "Enter name: ";
	getline(cin, student.name);
	cout << "Enter score 1: ";
	cin >> student.scores[0];
	cout << "Enter score 2: ";
	cin >> student.scores[1];
	cout << "Enter score 3: ";
	cin >> student.scores[2];
	cout << "Enter score 4: ";
	cin >> student.scores[3];
	cout << "Enter course: ";
	cin >> student.course;
	cout << endl;

	return in;
}
