#pragma once
#include <iostream>
#include <string>
using namespace std;
class Student
{
	int studentID;
	string title;
	string name;
	int* scores;
	string course;
public:
	Student(int studentID, string title, string name, int s1, int s2, int s3, int s4, string course);
	Student();
	Student(const Student& source);
	Student& operator= (const Student& otherStudent);
	int operator== (Student& otherStudent);
	int operator != (Student& otherStudent);

	int getStudentID();
	string getTitle();
	string getName();
	int* getScores();
	string getCourse();

	void setStudentID(int studentID);
	void setTitle(string title);
	void setName(string name);
	void setScores(int s1, int s2, int s3, int s4);
	void setCourse(string course);
	
	friend ostream& operator<< (ostream& out, const Student& student);
	friend istream& operator>> (istream& in, Student& student);
	~Student();
};
